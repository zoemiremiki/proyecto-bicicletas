class Bicicleta {
  constructor(id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
  }
}

Bicicleta.prototype.toString = () => {
  return `id: ${this.id} | color: ${this.color}`;
};

// store the bikes
Bicicleta.allBicis = [];
// push the bikes
Bicicleta.add = (aBici) => {
  Bicicleta.allBicis.push(aBici);
};

// return the bike by id
Bicicleta.findById = (aBici) => {
  const bici = Bicicleta.allBicis.find((bici) => bici.id == aBici);
  if (!bici) throw new Error(`bici con id ${aBici} no existe`);
  return bici;
};

// remove the bike with the given id
Bicicleta.removeById = (aBiciId) => {
  if (!Bicicleta.findById(aBiciId)) return;

  const aBiciIndex = Bicicleta.allBicis.findIndex((bici) => {
    return bici.id == aBiciId;
  });
  Bicicleta.allBicis.splice(aBiciIndex, 1);
};

// create hardcoded bikes
let bici1 = new Bicicleta(1, "rojo", "urbana", [-34.6012424, -58.3861497]);
let bici2 = new Bicicleta(2, "azul", "urbana", [-34.596932, -58.3808287]);

// add them
Bicicleta.add(bici1);
Bicicleta.add(bici2);

module.exports = Bicicleta;
