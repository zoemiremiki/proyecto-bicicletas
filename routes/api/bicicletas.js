const express = require("express");
const router = express.Router();
const bicicletaController = require("../../controllers/api/bicicletaControllerAPI");

router.get("/", bicicletaController.bicicleta_list);
router.post("/create", bicicletaController.bicicleta_create);
router.delete("/:id/delete", bicicletaController.bicicleta_deleteById);
router.post("/:id/update", bicicletaController.bicicleta_updateById);

module.exports = router;
