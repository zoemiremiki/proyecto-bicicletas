const Bicicleta = require("../../models/bicicleta");

exports.bicicleta_list = (req, res) => {
  res.status(200).json({
    bicicletas: Bicicleta.allBicis,
  });
};

exports.bicicleta_create = (req, res) => {
  const { id, color, modelo, ...ubicacion } = req.body;
  const bici = new Bicicleta(id, color, modelo, [
    ubicacion.long,
    ubicacion.lat,
  ]);
  Bicicleta.add(bici);

  res.status(200).json({
    bicicleta: bici,
  });
};

exports.bicicleta_deleteById = (req, res) => {
  Bicicleta.removeById(req.params.id);

  res.status(204).send();
};

exports.bicicleta_updateById = (req, res) => {
  const bici = Bicicleta.findById(req.params.id);
  bici.color = req.body.color;
  bici.modelo = req.body.modelo;
  bici.ubicacion = [req.body.long, req.body.lat];

  res.status(200).json({
    bicicleta: bici,
  });
};
