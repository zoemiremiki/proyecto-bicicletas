const Bicicleta = require("../models/bicicleta");

exports.bicileta_list = (req, res) => {
  res.render("bicicletas/index", { bicis: Bicicleta.allBicis });
};

exports.bicileta_create_get = (req, res) => {
  res.render("bicicletas/create");
};
exports.bicileta_create_post = (req, res) => {
  const { id, color, modelo, ...ubicacion } = req.body;
  const bici = new Bicicleta(id, color, modelo, [
    ubicacion.long,
    ubicacion.lat,
  ]);
  Bicicleta.add(bici);

  res.redirect("/bicicletas");
};

exports.bicicleta_delete_post = (req, res) => {
  Bicicleta.removeById(req.params.id);

  res.redirect("/bicicletas");
};
exports.bicicleta_update_get = (req, res) => {
  const bici = Bicicleta.findById(req.params.id);
  res.render("bicicletas/update", { bici });
};
exports.bicicleta_update_post = (req, res) => {
  const bici = Bicicleta.findById(req.params.id);
  bici.color = req.body.color;
  bici.modelo = req.body.modelo;
  bici.ubicacion = [req.body.long, req.body.lat];
  res.redirect("/bicicletas");
};
